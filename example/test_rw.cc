/* vim: set cin ts=4 sw=4 tw=80: */
// test_rw.cc
//
// Copyright (C) 2014 - Atri Bhattacharya
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

// tabdatrw.hpp already includes iostream, so no need to manually include again
#include "../src/tabdatrw.hpp"
#include <string>
#include <fstream>
#include <boost/lexical_cast.hpp> // Used in the last demonstration

using namespace std;

int main()
{
	// example.dat is a two-column data file with lots of ugly string inserts for testing
	const string FILENAME= "./example/example.dat";
	
	// Example: Read from cin
	cout << "EXAMPLE 1: Reading from standard input" << endl
		 << "======================================" << endl
		 << "Enter 3-column data; reading ends with" << endl
		 << "CTRL-D as the input (in a single line)." << endl;
	tabdat T_cin(cin, 3);
	cout << "Printing data read from stdin:" << endl;
	T_cin.print();
	cout << endl;

	vvdouble v;
	// Exceptions are thrown when
	// * File not found, or any I/O error
	// * User asks for more columns than exists in file
	// * User calls irowdata/icoldata with r/c index more than available in data 
	//
	cout << "EXAMPLE 2: Reading from file" << endl
	     << "============================" << endl
		 << "File name: " << FILENAME      << endl;
	ifstream fin;
	try
	{
		fin.open(FILENAME, ios::in);
	}
	catch (ifstream::failure & e)
	{
		cerr << "ERROR: File " << FILENAME << " not found" << endl;
		terminate();
	}
	
	// input stream fin, 2 cols of data, 1 starting row skipped, 1 header line
	tabdat Tf(fin, 2, 1, 1);
	fin.close();
	// Print output to std display
	cout << "Printing full data as read:" << endl;
	Tf.print();

	cout << endl
		 << "Printing column 2 data individually:" << endl;

	// Get column 2 data individually and print them
	vecd c2 = Tf.col(1);
	for (vecd::iterator it2 = c2.begin(); it2 != c2.end(); ++it2)
		cout << *it2 << endl;

	cout << endl
		 << "Printing row 5 data individually:" << endl;

	vecd r5 = Tf.row(4);
	for (vecd::iterator it = r5.begin(); it != r5.end(); ++it)
	{
		cout << *it << '\t';
	}
	cout << endl;

	cout << endl
		 << "Trying to get data from non-existent row/col throws exception:"
		 << endl;

	try
	{
		icoldata(Tf.get_data(), INT_MAX);
	}
	catch (const indexerr & e)
	{
		cerr << e.what() << endl;
	}

	cout << endl
		 << "Print the header(s) from the data file:" << endl
		 << Tf.header()
		 << endl;

	return 0;
}
