tabdatrw
========
*A simple file read/write library for tabulated numeric data*

tabdatrw is a simple library that reads/writes numeric data formatted as columns
from an input stream and stores it in a vvdouble container. Numeric data must be
castable into the double data type. Lines starting with any non-numeric
character (other than SPACE and TAB) and blank lines are automatically ignored.
If a line starts with SPACE or TAB, it is scanned until valid data is found in
the line, or until it hits end-of-line.

Usage of the library functions is demonstrated in the example code
`test_rw.cc`.

## INSTALLATION ##
For detailed instructions read the `INSTALL` file.

Briefly, doing the following steps will install the library and headers in the
standard location (/usr/local/lib[64]/ and /usr/local/include/) respectively:

    ./configure
    make -j<n>
    make install

(where <n\> is an integer specifying the number of parallel make threads to run
during compilation, e.g. make -j4 on a quad-core processor)

To configure the installation location, e.g. to /usr instead of /usr/local,
the `--prefix` argument has to be passed to `./configure` appropriately. E.g.,  
`./configure --prefix=/usr`

The usual `make` and `make install` as above will then install the library and
header file to /usr/lib[64] and /usr/include respectively.

## HEADER FILES ##

The header file to include is **tabdatrw.hpp**; e.g., main.cc in this instance
will contain  

    #include <tabdatrw.hpp>

## LINKING TO tabdatrw##
To link to the installed library make use of the `pkg-config` utility.

E.g., to link a main.cc file, which uses functions from the tabdatrw library,
one does the following:  
``g++ main.cc `pkg-config --cflags --libs tabdatrw` ``
