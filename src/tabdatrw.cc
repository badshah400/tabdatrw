/* vim: set et cin ts=4 sw=4 tw=80: */
// tabdatrw.cc
//
// Copyright (C) 2014 - Atri Bhattacharya
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include <fstream>
#include <iomanip>
#include <exception>
#include <string>
#include <regex>
#include <limits>
#include "tabdatrw.hpp"

using namespace std;
typedef boost::tokenizer<boost::char_separator<char> > tokenizer;

tabdat::tabdat(std::istream & in,
               const unsigned int & ncol,
               const unsigned int & nrowskip,
               const unsigned int & nheaders)
    : _ncol(ncol), _nhead(nheaders), _nskiprow(nrowskip)
{
    using namespace std;
    string line;

    // String of characters used as separators
    boost::char_separator<char> sep(";| \t,");

    regex blankln("^\\s*$");  // To ignore blank lines

    unsigned int hcnt = 0, skipcnt = 0;
    while (in)
    {
        for (; skipcnt < this->_nskiprow; skipcnt++)
        {
            getline(in, line);
            continue;
        }
        for (; hcnt < this->_nhead; hcnt++)
        {
            getline(in, line);
            this->_header += line;
            this->_header += '\n'; // New line after each readline
            continue;
        }
        bool ignore_line = false;
        getline(in, line);
        if (regex_match(line, blankln))
        {
            ignore_line = true;
            continue;
        }

        // Initialise with NAN so that if some data is missing, it is
        // registered as NAN and not a valid data (like 0).
        vector<double> vv(_ncol, numeric_limits<double>::quiet_NaN());
        vector<double>::iterator vv_it = vv.begin();

        tokenizer tokens(line, sep);

        for (tokenizer::iterator tok_iter = tokens.begin();
             (distance(tokens.begin(),tok_iter) < this->_ncol) // Ensure at most ncol
                                                         // entries
              && (tok_iter!=tokens.end()); ++tok_iter)
        {
            try
            {
                *vv_it = stod(*tok_iter);
            }
            catch (const invalid_argument & e)
            {
                if (tok_iter==tokens.begin())
                {
                    // This means the line begins with a non-number
                    // Skip entire line in this case.
                    ignore_line = true;
                    break;
                }
                *vv_it = numeric_limits<double>::quiet_NaN();
            }
            ++vv_it;
        }
        if (!ignore_line)
        {
            this->_data.push_back(vv);
        }
    }
}

void tabdat::print(ostream & os, const bool & print_header)
{
    using namespace std;
    if (print_header)
        cout << this->header();
    const double lthres = 1e-3, uthres = 1e4;
    for (auto v : this->_data)
    {
        for (auto d : v)
        {
            os.setf(ios_base::fixed, ios_base::floatfield);
            os.precision(7);

            // For very small and very large numbers use scientific notation
            if ((abs(d) < lthres || abs(d) > uthres) &&
                abs(d) > numeric_limits<double>::min())
            {
                os.setf(ios_base::scientific, ios_base::floatfield);
                os.precision(4);
            }

            os << setw(25) << d;
        }
        os << endl;
    }

    return;
}

double tabdat::operator()(const unsigned int & r, const unsigned int & c)
{
    indexerr rowerr("row", this->_data.size()-1, r);
    if (r >= this->_data.size())
    {
        throw rowerr;
    }

    indexerr colerr("col", this->_data.at(0).size()-1, c);
    if (c >= this->_data.at(0).size())
    {
        throw colerr;
    }

    return this->_data.at(r).at(c);
}

vecd tabdat::row(const unsigned int & r)
{
    indexerr rowerr("tabdat::row", this->_data.size()-1, r);
    if (r >= this->_data.size())
    {
        throw rowerr;
    }
    return this->_data.at(r);
}

vecd tabdat::col(const unsigned int & c)
{
    vecd vcol;
    indexerr colerr("tabdat::col", this->_data.at(0).size()-1,c);
    if (c >= this->_data.at(0).size())
    {
        throw colerr;
    }
    for (vvdouble::const_iterator it = this->_data.begin();
            it != this->_data.end();
            ++it)
    {
        vcol.push_back(it->at(c));
    }
    return vcol;
}

vvdouble tabdatr(const string & fname, const unsigned int ncols)
{
    boost::char_separator<char> sep(";| \t,");
    regex blankln("^\\s*$");  // Ignore blank lines
    vvdouble dat;
    ifstream fin(fname.c_str());

    string line;

    while (fin)
    {
        bool ignore_line = false;
        std::getline(fin, line);
        if (regex_match(line, blankln))
        {
            ignore_line = true;
            continue;
        }

        // Initialise with NAN so that if some data is missing, it is
        // registered as NAN and not a valid data (like 0).
        vector<double> vv(ncols, numeric_limits<double>::quiet_NaN());
        vector<double>::iterator vv_it = vv.begin();

        tokenizer tokens(line, sep);

        for (tokenizer::iterator tok_iter = tokens.begin();
             (distance(tokens.begin(),tok_iter) < ncols) // Ensure at most ncol
                                                         // entries
              && (tok_iter!=tokens.end()); ++tok_iter)
        {
            try
            {
                *vv_it = stod(*tok_iter);
            }
            catch (const invalid_argument & e)
            {
                if (tok_iter==tokens.begin())
                {
                    // This means the line begins with a non-number
                    // Skip entire line in this case.
                    ignore_line = true;
                    break;
                }
                *vv_it = numeric_limits<double>::quiet_NaN();
            }
            ++vv_it;
        }
        if (!ignore_line)
        {
            dat.push_back(vv);
        }
    }

    fin.close();

    return dat;
}

void tabdatw(const vvdouble & v, ostream & os)
{
    const double lthres = 1e-3, uthres = 1e4;
    for (unsigned int i = 0; i < v.size(); i += 1)
    {
        for (unsigned int j = 0; j < v.at(i).size(); j += 1)
        {
            double ival = v.at(i).at(j);
            os.setf(ios_base::fixed, ios_base::floatfield);
            os.precision(7);

            // For very small and very large numbers use scientific notation
            if ((abs(ival) < lthres || abs(ival) > uthres) &&
                numeric_limits<double>::min())
            {
                os.setf(ios_base::scientific, ios_base::floatfield);
                os.precision(4);
            }

            if (j)
                os.width(25);

            os << ival;
        }
        os << endl;
    }

    return;
}

vecd irowdata(const vvdouble & v, const unsigned int & r)
{
    vecd vrow;
    indexerr rowerr("irowdata", v.size()-1, r);
    if (r >= v.size())
    {
        throw rowerr;
    }

    for (unsigned int i = 0; i < v.at(r).size(); i++)
    {
        vrow.push_back(val_at(v, r, i));
    }
    return vrow;
}

vecd icoldata(const vvdouble & v, const unsigned int & c)
{
    vecd vcol;
    indexerr colerr("icoldata", v.at(0).size()-1,c);
    if (c >= v.at(0).size())
    {
        throw colerr;
    }
    for (unsigned int i = 0; i < v.size(); i++)
    {
        vcol.push_back(val_at(v, i, c));
    }
    return vcol;
}
