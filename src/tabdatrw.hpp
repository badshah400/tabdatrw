/* vim: set ai et ts=4 sw=4 tw=80: */
/*
 * tabdatrw.hpp
 *
 * Copyright (C) 2014 - Atri Bhattacharya
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __TABDATRW_HPP__
#define __TABDATRW_HPP__

#include <iostream>
#include <vector>
#include <exception>
#include <string>
#include <boost/tokenizer.hpp>

typedef boost::tokenizer<boost::char_separator<char> > tokenizer;

/* undef vvdouble if it has already be defined, e.g., when using together
 * with interp2dpp library which also defines it exactly identically.
 */
#ifdef vvdouble
#undef vvdouble
#endif

typedef std::vector<double> vecd;
typedef std::vector<vecd> vvdouble;

// Exception handling for function irowdata and icoldata
// Throws exception message of the form:
// ---------------------------------------------------------------
//       tabdatrw ERROR: Maximum valid index for function foo: N
//                       Inde requested: I.
//
// ---------------------------------------------------------------
//
class indexerr : public std::exception
{
public:
    indexerr(const std::string & s, const unsigned int I, const unsigned int i)
        : _funcid(s), _validx(I), _reqidx(i)
    { }

    inline unsigned int max_index() const { return this->_validx; }
    inline unsigned int req_index() const { return this->_reqidx; }

    const char * what() const noexcept
    { return "tabdatrw ERROR: Invalid index requested."; }
private:
    std::string _funcid;
    unsigned int _validx;
    unsigned int _reqidx;
};

class tabdat
{
public:
    tabdat(std::istream & in,
           const unsigned int & ncol = 1,
           const unsigned int & nskiprow = 0,
           const unsigned int & nheaders = 0);
    void print(std::ostream & os = std::cout, const bool & print_header=false);
    vecd row(const unsigned int & r);
    vecd col(const unsigned int & c);
    double operator()(const unsigned int & r, const unsigned int & c);
    inline vvdouble get_data() const { return this->_data; }
    inline unsigned int nrows() const { return this->_data.size(); }
    inline unsigned int ncols() const { return this->_data.at(0).size(); }
    inline std::string header() const { return this->_header; }

private:
    unsigned int _ncol;
    unsigned int _nhead;
    unsigned int _nskiprow;
    vvdouble _data;	
    std::string _header;
};

void tabdatw(const vvdouble &, std::ostream & = std::cout);
vvdouble tabdatr(const std::string &, const unsigned int = 1);

inline double val_at(const vvdouble & v,
                     const unsigned int i,
                     const unsigned int j)
{
	return v.at(i).at(j);
}

vecd irowdata(const vvdouble &, const unsigned int &);
vecd icoldata(const vvdouble &, const unsigned int &);

#endif
